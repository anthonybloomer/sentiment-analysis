# sentiment-analysis
Sentiment analysis for a given twitter user.

This application uses the Twitter [Tweepy] (http://www.tweepy.org) API and the sentiment analysis API at [Text Processing] (http://text-processing.com)

## Instructions

1. You need to register the application with [Twitter](https://apps.twitter.com) 
2. Update config.py with your application keys.
3. Run ```pip install -r requirements.txt```
4. Run ```python app.py```
5. Open http://127.0.0.1:5000/

from flask import Flask, render_template, request
from sentiment import Sentiment
app = Flask(__name__)


@app.route("/submit", methods=['POST'])
def submit():
    if request.method == 'POST':
        if request.form['screen_name']:
            num_tweets = request.form['num_tweets']
            screen_name = request.form['screen_name']
            s = Sentiment(screen_name, num_tweets)
            results = s.analyse()
            return render_template('home.html', results=results, screen_name=screen_name)
        return render_template('home.html', error='Enter your search term')


@app.route("/")
def index():
    return render_template('home.html')


if __name__ == "__main__":
    app.run(debug=True)

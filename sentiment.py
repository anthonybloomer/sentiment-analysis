import requests
import tweepy
import config
import sys


class Sentiment:
    processing_api = 'http://text-processing.com/api/sentiment/'

    def __init__(self, screen_name, num_tweets = 250, language='english'):
        self.screen_name = screen_name
        self.language = language
        self.num_tweets = num_tweets
        self.twitter_auth = tweepy.OAuthHandler(config.consumer_key, config.consumer_secret)
        self.twitter_auth.set_access_token(config.access_key, config.access_secret)
        self.twitter_api = tweepy.API(self.twitter_auth, wait_on_rate_limit=True, wait_on_rate_limit_notify=True)

    def _call(self, tweet):
        r = requests.post(self.processing_api, data={'language': self.language, 'text': tweet})
        if 'Throttled' in r.content:
            print r.content
            sys.exit()
        else:
            return r.json()

    def _get_sentiment(self, tweet):
        sentiment = self._call(tweet)
        return sentiment['label']

    def analyse(self):
        print 'Getting tweets, this may take a while...'
        try:
            tweets = self._get_tweets()
            results = []
            print 'Totalling tweet sentiment'
            for tweet in tweets:
                results.append({'tweet': tweet.text, 'sentiment': self._get_sentiment(tweet.text)})
            return results
        except tweepy.error.TweepError:
            pass

    def _get_tweets(self):
        tweets = self.twitter_api.user_timeline(screen_name=self.screen_name, count=self.num_tweets, include_rts=False)
        return tweets
